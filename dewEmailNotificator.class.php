<?php
/*
 * This file is part of Domain Expiration Watcher
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Domain Expiration Watcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Domain Expiration Watcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Domain Expiration Watcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */

class dewEmailNotificator
{
	private $url = "http://probeta.net/~sim6/domain_expiration_watcher/";
	private $email_headers = "Bcc: sim6@graciasensefils.net\r\nFrom: Domain expiration watcher <sim6@graciasensefils.net>\r\nX-Sender: domain-expiration-watcher\r\nContent-Type: text/plain; charset=utf8\r\n";
	private $powered_by = "Powered by Domain Expiration Watcher: https://gitlab.com/sim6/domain_expiration_watcher";
	private $subject;
	private $message;
	private $email;
	
	public function sendEmailConfirmationEmailDomain($email, $domain, $action, $hash)
	{
		$this->email = $email;
		$this->subject = "Domain expiration watcher confirmation action";
		$this->message = "\n\nYou can confirm " . $action . " to " . $domain . " expiration notifications by clicking the link below:\n";
		$this->message .= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?confirmation=" . $hash . "\n\n";
		return $this->sendEmail();
	}
	
	public function sendEmailNotificationSubscribeEmailDomain($email,$domain)
	{
		$this->email = $email;
		require_once("DomainExpirationWatcher.class.php");
		$domain_expiration_watcher = new DomainExpirationWatcher();
		$expiration_date = date('c', $domain_expiration_watcher->getDomainExpirationTime($domain));
		$expiration_left = $domain_expiration_watcher->getDaysToDomainExpiration($domain);
		$next_notification = date('c', $domain_expiration_watcher->getNextNotificationTime($domain));
		$next_notification = date('c', $domain_expiration_watcher->database->getNextNotificationEmailDomain($this->email, $this->domain));
		echo "AA".$domain_expiration_watcher->database->getNextNotificationEmailDomain($this->email, $this->domain)."BB";

		$this->subject = "Subscribe domain expiration watcher notification";
		$this->message = "\n\nYou are subscribed to " . $domain . " expiration notifications.\n";
		$this->message .= "This domain expire on " . $expiration_left . " days.\n";
		$this->message .= "Expiration date: " . $expiration_date . "\n";
//		$next_notification_time = strtotime("+" . $domain_expiration_watcher->notification_times[$domain_expiration_watcher->notifications[$email][$domain]]);
//		$this->message .= "Next notification: " . date('c', $next_notification_time) . "\n";
		$this->message .= "You will receive new notifications when approaching this date.\n";
		$this->message .= "Next notification on " . $next_notification . "\n";
		$this->message .= "You can unsubscribe by clicking the link below:\n";
		$this->message .= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?email=" . $email . "&domain=" . $domain . "&action=unsubscribe\n\n";
		$this->message .= "Welcome!\n\n";
		return $this->sendEmail();
	}
	
	public function sendEmailNotificationUnsubscribeEmailDomain($email,$domain)
	{
		$this->email = $email;
		$this->subject = "Unsubscribe domain expiration watcher notification";
		$this->message = "\n\nYou are unsubscribed to " . $domain . " expiration notifications.\n";
		$this->message .= "You can subscribe again by clicking the link below:\n";
		$this->message .= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?email=" . $email . "&domain=" . $domain . "&action=subscribe\n\n";
		$this->message .= "See you!\n\n";
		return $this->sendEmail();
	}


	public function sendDomainExpirationDateChangedNotification($domain,$new_expiration_date,$old_expiration_date,$email)
	{
		$this->email = $email;
		$this->subject = "Notification from domain expiration watcher";
		$this->message = "\nThe domain " . $domain . " was changed their expiration date from " . date('c', $old_expiration_date) . " to " . date('c', $new_expiration_date) . ".\n";
		$this->sendEmail();
	}
	
	public function sendNotifications($notifications, $expirations)
	{
		foreach($notifications as $email => $domains)
		{
			$this->email = $email;
			$this->subject = "";
			$this->message = "";
			foreach($domains as $domain)
			{
				if(!empty($this->message))
				{
					$this->message .= "\n  ======\n\n\n";
				}
				if($expirations[$domain]['period']['expiration'] == '1day') 
				{
					$this->subject = "WARNING ";
					$this->message .= "WARNING ";
				}
				if($expirations[$domain]['period']['expiration'] == '1week' || $expirations[$domain]['period']['expiration'] == '2week')

				{
					$this->subject = "ALERT ";
					$this->message .= "ALERT ";
				}
				if($expirations[$domain]['period']['expiration'] == 'now')
				{
					$this->subject = "EXPIRED!! ";
					$this->message .= "EXPIRED!! Domain was expired ";
				}
				else
				{
					$this->message .= "Less than " . $expirations[$domain]['period']['expiration'] . " to expire domain ";
				}
				if(!empty($expirations[$domain]['period']['expiration']))
				{
					$expiration_date = date('c', $expirations[$domain]['time']);
					$expiration_left = $expirations[$domain]['days'];
					$next_notification = date('c', $expirations[$domain]['next_notification']);

					$this->message .= $domain . ", expire on " . $expiration_left . " days.\n";
					$this->message .= "Expiration date: " . $expiration_date . "\n\n";
					$this->message .= "Next notification: " . $next_notification . "\n";
					$this->message .= "If you wish to unsubscribe to these notifications click the link below:\n";
					$this->message .= $this->url . "?email=" . $email . "&domain=" . $domain . "&action=unsubscribe\n\n";
				}
			}
			if(!empty($this->message))
			{
				$this->subject .= "Notification from domain expiration watcher";
				$this->sendEmail();
			}
		}
	}

	private function sendEmail()
	{
		if(!empty($this->message) && !empty($this->subject))
		{
			$this->message = "\n\n" . $this->message . "\n\n " . $this->powered_by . "\n";
			return mail($this->email, $this->subject, $this->message, $this->email_headers);	
		}
	}

}

?>
