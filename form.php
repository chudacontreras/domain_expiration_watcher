<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
/*
 * This file is part of Domain Expiration Watcher
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Domain Expiration Watcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Domain Expiration Watcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Domain Expiration Watcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Domain Expiration Watcher</title>
		<style type="text/css">
			body
			{
				background-color: #eee;
			}
			h1
			{
				color: grey;
				text-align: center;
			}

			fieldset
			{
				background-color: white;
				margin: auto;
				width: 21em;
				border-radius: 1em;
				-moz-border-radius: 1em;
				box-shadow: inset 0 0 5px 5px #eee;
				-moz-box-shadow: inset 0 0 5px 5px #eee;
				-webkit-box-shadow: inset 0 0 5px 5px #eee;
			}

			label
			{
				margin: 0.5em 0.5em;
				float: left;
				text-align: right;
				width: 6em;
			}

			.radio label
			{
				margin: 0.5em 0.5em;
				float: right;
				text-align: left;
				width: 15em;
			}

			input
			{
				margin: 0.5em 1em;
				float: right;
			}

			.radio input
			{
				margin: 0.75em 1em;
				margin-right: 1em;
			}

			.submit input
			{
				float: none;
			}

			.submit
			{
				clear: both;
				text-align: center;
			}

			#message
			{
				background-color: white;
				padding: 1em;
				margin: 2em auto;
				width: 21em;
				border-radius: 1em;
				-moz-border-radius: 1em;
				-webkit-border-radius: 1em;
				box-shadow: inset 0 0 5px 5px #eee;
				-moz-box-shadow: inset 0 0 5px 5px #eee;
				-webkit-box-shadow: inset 0 0 5px 5px #eee;
			}

			#footer
			{
				margin-top: 1em;
				text-align: center;
				opacity: 0.5;
			}

			#footer img
			{
				margin-top: 0.25em;
				border:none;
				height:25px;
			}
		</style>
	</head>
	<body>
		<h1>Domain Expiration Watcher</h1>
		<p id="message">
			<?php echo $message ?>
		</p>
		<form action="">
			<fieldset>
				<legend>Get expiration notifications for a domain</legend>
				<div>
					<label for="email">Email:</label>
					<input type="text" id="email" name="email" />
				</div>
				<div>
					<label for="domain">Domain:</label>
					<input type="text" id="domain" name="domain" />
				</div>
				<div class="radio">
					<label for="subscribe">Subscribe</label>
					<input type="radio" id="subscribe" name="action" value="subscribe" checked="checked" />
				</div>
				<div class="radio">
					<label for="unsubscribe">Unsubscribe</label>
					<input type="radio" id="unsubscribe" name="action" value="unsubscribe" />
				</div>
				<div class="radio">
					<label for="show">Only show expiration date</label>
					<input type="radio" id="show" name="action" value="show" />
				</div>
				<div class="submit">
					<input type="submit" value="Go" />
				</div>
			</fieldset>
		</form>
		<div id="footer">
			<div>
				Powered by <a href="https://gitlab.com/sim6/domain_expiration_watcher">Domain Expiration Watcher</a>
			</div>
			<div>
				<a href="http://www.gnu.org/licenses/agpl-3.0.html"><img src="http://www.gnu.org/graphics/agplv3-155x51.png" alt="AGPLv3" /></a>
				<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Strict" style="border:none; height:25px;" /></a>
				<a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3"><img alt="Valid CSS!" src="http://www.w3.org/Icons/valid-css-blue.png" style="border:none ; height:25px;" /></a>
				<a href="http://www.w3.org/WAI/WCAG1A-Conformance"><img alt="Nivel A de las Directrices de Accesibilidad para el Contenido Web 1.0 del W3C-WAI" src="http://www.w3.org/WAI/wcag1A-blue" style="border:none ; height:25px" /></a>
			</div>
		</div>
	</body>
</html>
