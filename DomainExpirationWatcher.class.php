<?php
/*
 * This file is part of Domain Expiration Watcher
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Domain Expiration Watcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Domain Expiration Watcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Domain Expiration Watcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */

class DomainExpirationWatcher
{
	private $notification_times = array (
			// ascending expiration period => next notification period
						'now' => '1hour',
						'1day' => '1hour',
						'1week' => '1day',
						'2week' => '3day',
						'1month' => '1week',
						'2month' => '2week', 
						'1year'	=> '2month',
//						'2year' => '4month',
	);
	private $database_type = "dewFileDataBase";
	private $notificator_type = "dewEmailNotificator";
	private $whois_server = 'whois-servers.net';
	private $whois_sleep = 15;
	private $delay = "3hour";
	private	$expirations = array();
	/*
		array(
			"domain1.tld" => array(
				time => 123456789,
				days => 44,
				period => array(
					expiration => 2month,
					notification => 1week
				),
				next_notification => 123456789	
			),
			"domain2.tld => ...
		)
	 */
	private $notifications = array();
	/*
		array(
			"e@mail1.tld" => array (
				"domain1.tld",
				"domain2.tld"
			)
			"e@mail2.tld" => ...
		)
	*/
	private $notificator;
	public $database;

	public function __construct()
	{
		require_once($this->database_type . ".class.php");
		$this->database = new $this->database_type;
		require_once($this->notificator_type . ".class.php");
		$this->notificator = new $this->notificator_type;
	}

	public function getDomainExpirationTime($domain)
	{
		if(!isset($this->expirations[$domain]['time'])){
			$server = end(explode(".", $domain)) . "." . $this->whois_server;
			$fp = @fsockopen($server, 43, $errno, $errstr, 30);
			if(!$fp)
			{
				echo "\nError - could not open a connection to $server\n $errstr ($errno)\n";
			}
			else
			{
				@fputs($fp, $domain."\r\n");
				@socket_set_timeout($fp, 128);
				while( !@feof($fp) ){
					$data[] = @fgets($fp);
				}
				@fclose($fp);
				// PHP >= 5.3.0
				//$match = preg_filter(array("/   Expiration Date: .*/"), array("$0"), $data);
				if(preg_match("/.*\.(org|info)/",$domain))
				{
					$expire_regexpr="Registry Expiry Date";
				}
				else
				{
					$expire_regexpr=".*Expiration Date";
				}
				$grep = preg_grep("/".$expire_regexpr.":.*/", $data);
				if(empty($grep))
				{
					echo "Error getting expiration time for $domain.\n";
					print_r($data);
				}
				$match = preg_replace("/".$expire_regexpr.":(.*)/", "$1", $grep);
				$this->expirations[$domain]['time'] = strtotime(implode("",$match));
				$this->getDaysToDomainExpiration($domain);
				$this->getNextNotificationTime($domain);
			}
		}
		if(isset($this->expirations[$domain]['time'])){
			return $this->expirations[$domain]['time'];
		}
	}

	public function getDaysToDomainExpiration($domain)
	{
		$this->getDomainExpirationTime($domain);
		$this->expirations[$domain]['days'] = round(( $this->expirations[$domain]['time'] - time() ) / ( 60 * 60 * 24 ));
		return $this->expirations[$domain]['days'];
	}

	public function getNextNotificationTime($domain)
	{
		if(!isset($this->expirations[$domain]['next_notification']))
		{
			$this->getDomainExpirationTime($domain);
			foreach($this->notification_times as $period['expiration'] => $period['notification'])
			{
				if($this->expirations[$domain]['time'] <= strtotime("+" . $period['expiration']))
				{
					$next_notification_time = strtotime("+" . $period['notification']);
					$start_next_expiration_period_time = strtotime("-" . $last_period['expiration'], $this->expirations[$domain]['time']);
					if($next_notification_time > $start_next_expiration_period_time)
					{
						if($start_next_expiration_period_time < time())
						{
							$next_notification_time = strtotime("+" . $last_period['notification']);
						}
						else
						{
							$next_notification_time = $start_next_expiration_period_time;
						}
					}
					$this->expirations[$domain]['next_notification'] = $next_notification_time;
					$this->expirations[$domain]['period']['notification'] = $period['notification'];
					$this->expirations[$domain]['period']['expiration'] = $period['expiration'];
					break;
				}
				$last_period = $period;
			}
			if(!isset($this->expirations[$domain]['next_notification']))
			{
				$this->expirations[$domain]['next_notification'] = strtotime("-" . $last_period['expiration'], $this->expirations[$domain]['time']);
			}
		}
		return $this->expirations[$domain]['next_notification'];
	}

	private function domainNotification($email, $domain)
	{
		$this->getDomainExpirationTime($domain);

		$old_domain_expiration_time = $this->database->getSavedDomainExpirationDate($domain);
		if($old_domain_expiration_time && $old_domain_expiration_time != $this->expirations[$domain]['time'])
		{
			$this->notificator->sendDomainExpirationDateChangedNotification($domain,$this->expirations[$domain]['time'],$old_domain_expiration_time,$email);
			$this->database->saveDomainExpirationDate($domain,$this->expirations[$domain]['time']);
		}
		else
		{
			$next_notification = $this->database->getNextNotificationEmailDomain($email, $domain);
			if(empty($next_notification)) $next_notification = $this->expirations[$domain]['next_notification'];
				
			if(!empty($next_notification) && strtotime("+" . $this->delay) >= $next_notification)
			{
				if(!empty($this->expirations[$domain]['next_notification']))
				{
					$this->notifications[$email][] = $domain;
					$this->database->setNextNotificationEmailDomain($email, $domain, $this->expirations[$domain]['next_notification']);
				}
			}
		}
	}

	public function emailDomain($email,$domain)
	{
		$this->domainNotification($email, $domain);
		$this->notificator->sendNotifications($this->notifications, $this->expirations);
	}

	public function all()
	{
		$emails = $this->database->getEmails();
		foreach ($emails as $email)
		{
			$domains = $this->database->getEmailDomains($email);
			foreach ($domains as $domain)
			{
				$this->domainNotification($email, $domain);
				sleep($this->whois_sleep);
			}
		}
		$this->notificator->sendNotifications($this->notifications, $this->expirations);
	}
}

?>
